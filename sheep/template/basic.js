export default {
  tabbar: {
    mode: 1,
    layout: 1,
    inactiveColor: '#8C8C8C',
    activeColor: '#FF7A0C',
    list: [
      {
        inactiveIcon: '/storage/decorate/20221115/76574bb482da1fd62539c0253f0152d6.png',
        activeIcon: '/storage/decorate/20221115/c92d07a10eec8850ab9a481638e5159b.gif',
        url: '/pages/index/index',
        text: '首页',
      },
      {
        inactiveIcon: '/storage/decorate/20221115/3fbe0412147a5d7f1c3116219ed39d32.png',
        activeIcon: '/storage/decorate/20221115/4ce2fdff833f8a37de9a6d760deee994.gif',
        url: '/pages/index/category?id=21',
        text: '分类',
      },
      {
        inactiveIcon: '/storage/decorate/20221115/113eb988efc1fe17cb90cdf5311e5b63.png',
        activeIcon: '/storage/decorate/20221115/2a3992a220f306d129ac1659304694bd.gif',
        url: '/pages/index/cart',
        text: '购物车',
      },
      {
        inactiveIcon: '/storage/decorate/20221115/f930c59d338a97a158ee53cb65bde082.png',
        activeIcon: '/storage/decorate/20221115/1130ab158b1c31b1e1d356fa78bb5f4a.gif',
        url: '/pages/index/user',
        text: '我的',
      },
    ],
    background: {
      type: 'color',
      bgImage: '',
      bgColor: '#FFFFFF',
    },
  },
  floatMenu: {
    show: 0,
    mode: 1,
    isText: 0,
    list: [
      {
        src: '',
        url: '',
        title: {
          text: '',
          color: '',
        },
      },
    ],
  },
  popupImage: {
    list: [
      {
        src: '/storage/decorate/20221115/6bfd03d0ad7f3d7f6ba7494c903cdc0c.png',
        url: '/pages/index/category?id=21',
        show: 2,
      },
    ],
  },
  theme: 'orange',
};
