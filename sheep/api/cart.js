import request from '@/sheep/request';
import { adapterCart } from '../helper/adapter';

export default {
  list: (data) =>
    request({
      url: '/frontend/cart/info',
      method: 'GET',
      custom: {
        showLoading: false,
        auth: true,
        adapter: (resp) => ({ ...resp, data: resp.data.map(adapterCart) }),
      },
    }),
  append: (data) =>
    request({
      url: '/frontend/cart/add',
      method: 'POST',
      custom: {
        showSuccess: true,
        successMsg: '已添加到购物车~',
      },
      data: {
        ...data,
        type: 'inc',
      },
    }),
  // 删除购物车
  delete: (ids) =>
    request({
      url: '/frontend/cart/' + ids,
      method: 'DELETE',
    }),
  update: (data) =>
    request({
      url: '/frontend/cart/update',
      method: 'POST',
      data: {
        ...data,
        type: 'cover',
      },
    }),
};
